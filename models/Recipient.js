const mongoose = require('mongoose');

const { Schema } = mongoose;

const recipientSchema = new Schema({
    position:  String, // должность
    nameDt: String, // ФИО в дательном падеже
    address:   String, //адрес
    recipientIm: String, //Имя и отчество в именительном падеже
    sex: Number, //Пол
    date: { type: Date, default: Date.now }
});

mongoose.model('Recipient', recipientSchema);